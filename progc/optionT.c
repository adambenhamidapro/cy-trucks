//PréIng2 MI2 Semestre 1, Projet CY-Trucks : Traitement -t
//#include "data.csv"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>     //pour la fct sleep()
#define SIZE 100

typedef struct _tree {
    char ville[SIZE];
    int nb_apparition_ville_depart_trajet;        //on compte seulement les villes de départ de chaque trajet (chaque trajet a plusieurs étapes)
    int nb_apparition_ville_arrivee_etape;        //on compte les villes d'arrivée de chaque étape de trajet
    int nb_apparition_par_trajet;
    // nb_apparition_par_trajet = nb_apparition_ville_depart_trajet + nb_apparition_ville_arrivee_etape - les doublons de ville par trajet
    struct _tree* pL;
    struct _tree* pR;
    int equilibre;                                  //equilibre = hauteur sous-arbre droit - hauteur sous-arbre gauche
} Tree;


Tree* createTree(char nom[]){
    Tree* pNew = malloc(sizeof(Tree));
    if(pNew == NULL){
        printf("Erreur 1 : pointeur NULL.\n");
        exit(1);
    }
    int i = 0;
    while(nom[i] != '\0'){
        if (i >= 99){
            printf("ERREUR 2 : Nom de ville de plus de 99 caractères.\n");
            exit(2);
        }
        pNew->ville[i] = *(nom + i);
        i++;
    }
    pNew->nb_apparition_ville_depart_trajet = 0;
    pNew->nb_apparition_ville_arrivee_etape = 0;
    pNew->nb_apparition_par_trajet = 0;
    pNew->equilibre = 0;
    pNew->pL = NULL;
    pNew->pR = NULL;
    return pNew;
}

//----------------- ROTATIONS SIMPLES -----------------
Tree* RotationGauche(Tree* p){
    if (p==NULL || p->pR == NULL){
        exit(10);
    }
    Tree* pivot = p->pR;

    //Equilibrage
    if(p->equilibre == 2){
        if(pivot->equilibre == 0){
            pivot->equilibre = -1;
            p->equilibre = 1;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = 0;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 2){
            pivot->equilibre = 0;
            p->equilibre = -1;
        }
        else{
            printf("Cas non pris en charge 11. \n");
            exit(11);
        }
    }

    else if(p->equilibre == 1){
        if(pivot->equilibre == -1){
            pivot->equilibre = -2;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 0){
            pivot->equilibre = -1;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = -1;
            p->equilibre = -1;
        }
        else{
            printf("Cas non pris en charge 12. \n");
            exit(12);
        }
    }

    else{
        printf("Cas non pris en charge 13. \n");
        exit(13);
    }

    //Rotation
    p->pR = pivot->pL;
    pivot->pL = p;
    if (pivot == NULL){
        printf("Erreur 14 : pointeur NULL.\n");
        exit(14);
    }

    return pivot;
}


Tree* RotationDroite(Tree* p){
    if (p==NULL || p->pL == NULL){
        exit(15);
    }
    Tree* pivot = p->pL;

    //Equilibrage
    if(p->equilibre == -2){
        //Rotation simple 1
        if(pivot->equilibre == -1){
            pivot->equilibre = 0;
            p->equilibre = 0;
        }
        //Rotation Simple 2
        else if(pivot->equilibre == 0){
            pivot->equilibre = 1;
            p->equilibre = -1;
        }
        else if(pivot->equilibre == -2){
            pivot->equilibre = 0;
            p->equilibre = 1;
        }
        else{
            printf("Cas non pris en charge 16. \n");
            exit(16);
        }
    }

    else if(p->equilibre == -1){
        if(pivot->equilibre == -1){
            pivot->equilibre = 1;
            p->equilibre = 1;
        }
        else if(pivot->equilibre == 0){
            pivot->equilibre = 1;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = 2;
            p->equilibre = 0;
        }
        else{
            printf("Cas non pris en charge 17. \n");
            exit(17);
        }
    }

    else{
        printf("Cas non pris en charge 18. \n");
        exit(18);
    }

    //Rotation
    p->pL = pivot->pR;
    pivot->pR = p;
    if (pivot == NULL){
        printf("Erreur 19 : pointeur NULL.\n");
        exit(19);
    }

    return pivot;
}


//----------------- DOUBLES ROTATIONS -----------------
Tree* DoubleRotationGauche(Tree* p){
    if (p==NULL || p->pR==NULL || p->equilibre!=2 || p->pR->equilibre!=-1){
        printf("Cas non pris en charge 20 : pointeurs NULL ou facteurs d'équilibre incorrectes.\n");
        exit(20);
    }

    p->pR = RotationDroite(p->pR);
    if (p->pR == NULL){
        printf("Erreur 21 : pointeur NULL.\n");
        exit(21);
    }
    p = RotationGauche(p);
    if (p == NULL){
        printf("Erreur 22 : pointeur NULL.\n");
        exit(22);
    }
    return p;
}

Tree* DoubleRotationDroite(Tree* p){
    if (p==NULL || p->pL==NULL || p->equilibre!=-2 || p->pL->equilibre!=1){
        printf("Cas non pris en charge 30 : pointeurs NULL ou facteurs d'équilibre incorrectes.\n");
        exit(30);
    }

    p->pL = RotationGauche(p->pL);
    if (p->pL == NULL){
        printf("Erreur 31 : pointeur NULL.\n");
        exit(31);
    }

    p = RotationDroite(p);
    if (p == NULL){
        printf("Erreur 32 : pointeur NULL.\n");
        exit(32);
    }
    return p;
}

//----------------- EQUILIBRAGE -----------------
Tree* equilibrerAVL(Tree* p){
    if (p == NULL){
        printf("Erreur 40 : pointeur NULL.\n");
        exit(40);
    }

    if (p->equilibre == 2){
        if (p->pR == NULL){
            printf("Erreur 41 : pointeur NULL.\n");
            exit(41);
        }

        //Double rotation Gauche (RD puis RG)
        if (p->pR->equilibre == -1){
            p = DoubleRotationGauche(p);
        }

        //Rotation Simple Gauche
        else if (p->pR->equilibre >= 0){
            p = RotationGauche(p);
        }

        else {
            printf("Cas non pris en charge 42.\n");
            exit(42);
        }
    }
    else if (p->equilibre == -2) {
        if (p->pL == NULL){
            printf("Erreur 43 : pointeur NULL.\n");
            exit(43);
        }

        //Double rotation Droite (RG puis RD)
        if (p->pL->equilibre == 1){
            p = DoubleRotationDroite(p);
        }

        //Rotation Simple Droite
        else if (p->pL->equilibre <= 0){
            p = RotationDroite(p);
        }

        else {
            printf("Cas non pris en charge 44.\n");
            exit(44);
        }
    }
    else if (p->equilibre>-2 && p->equilibre<2){        //p->equilibre == -1 ou 0 ou 1. Donc équilibré.
        //do nothing
        //printf("Pas besoin de rééquilibrage : equilibre = %d.\n", p->equilibre);
    }
    else{
        //do nothing
        printf("Cas non pris en charge 45 : racine de l'arbre >2 ou <-2 .\n");
        exit(45);
    }
    if (p == NULL){
        printf("Erreur 46 : pointeur NULL.\n");
        exit(46);
    }

    return p;
}


//---------------------------------- INSERTION AVL ----------------------------------

// AVL trié avec des entiers (par nb d'apparition de ville)
Tree* insertAVL_numerique (Tree* p, Tree* p_copie, int* h){
    //TEST
    if (h == NULL){
        printf("Erreur 50 : pointeur NULL.\n");
        exit(50);
    }

    //process
    if (p == NULL){
        p = p_copie;
        *h = 1;
        if (p == NULL){
            printf("Erreur 51 : pointeur NULL.\n");
            exit(51);
        }
        //printf("TEST, value = %d,   eq = %d.\n", p->value, p->equilibre);
        return p;
    }
    else if (p_copie->nb_apparition_par_trajet >= p->nb_apparition_par_trajet){
        p->pR = insertAVL_numerique(p->pR, p_copie, h);
    }
    else if (p_copie->nb_apparition_par_trajet < p->nb_apparition_par_trajet){
        p->pL = insertAVL_numerique(p->pL, p_copie, h);
        *h = -*h;
    }
    else {
        printf("Erreur 52 : Cas impossible.\n");
        exit(52);
    }

    if (*h != 0){
        p->equilibre = p->equilibre + *h;
        //printf("TEST, value = %d,   eq = %d.\n", p->value, p->equilibre);
        p = equilibrerAVL(p);
        if(p->equilibre == 0){
            *h = 0;
        }
        else {
            *h = 1;
        }
    }

    // return the address (may be new)
    return p;
}

// AVL trié par chaînes de caractères (ordre alphabétique de nom de ville)
Tree* insertAVL_alphabetique (Tree* p, char nom_ville[], int ville_Arrivee, int* h){
    /*  ville_Arrivee == 1 signifie : on insère la ville de départ du trajet (ville départ de l'étape 1)
        ville_Arrivee == 2 signifie : on insère la ville d'arrivée de toute étape >= 1
    */
    //TEST
    if (h == NULL){
        printf("Erreur 50 : pointeur NULL.\n");
        exit(50);
    }

    //process
    if (p == NULL){
        p = createTree(nom_ville);
        if (ville_Arrivee == 1){
            p->nb_apparition_ville_depart_trajet = p->nb_apparition_ville_depart_trajet + 1 ;
            p->nb_apparition_par_trajet = p->nb_apparition_par_trajet + 1 ;
        }
        else if (ville_Arrivee == 2){
            p->nb_apparition_ville_arrivee_etape = p->nb_apparition_ville_arrivee_etape + 1 ;
            p->nb_apparition_par_trajet = p->nb_apparition_par_trajet + 1 ;
        }
        else{
            printf("Erreur 51 : on a ni une ville de départ, ni une ville d'arrivée.\n");
            exit(51);
        }
        *h = 1;
        if (p == NULL){
            printf("Erreur 52 : pointeur NULL.\n");
            exit(52);
        }
        //printf("TEST, value = %d,   eq = %d.\n", p->value, p->equilibre);
        return p;
    }
    else if (strcmp(nom_ville, p->ville) > 0){
        p->pR = insertAVL_alphabetique(p->pR, nom_ville, ville_Arrivee, h);
    }
    else if (strcmp(nom_ville, p->ville) < 0){
        p->pL = insertAVL_alphabetique(p->pL, nom_ville, ville_Arrivee, h);
        *h = -*h;
    }
    else {
        // On a trouvé le même nom de ville : on ne fait rien
        // strcmp(nom, p->ville) == 0
        if (ville_Arrivee == 1){
            p->nb_apparition_ville_depart_trajet = p->nb_apparition_ville_depart_trajet + 1 ;
            p->nb_apparition_par_trajet = p->nb_apparition_par_trajet + 1 ;
        }
        else if (ville_Arrivee == 2){
            p->nb_apparition_ville_arrivee_etape = p->nb_apparition_ville_arrivee_etape + 1 ;
            p->nb_apparition_par_trajet = p->nb_apparition_par_trajet + 1 ;
        }
        else{
            printf("Erreur 53 : on a ni une ville de départ, ni une ville d'arrivée.\n");
            exit(53);
        }
        return p;
    }

    if (*h != 0){
        p->equilibre = p->equilibre + *h;
        p = equilibrerAVL(p);
        if(p->equilibre == 0){
            *h = 0;
        }
        else {
            *h = 1;
        }
    }

    // return the address (may be new)
    return p;
}

Tree* selection_insertAVL_alphabetique (Tree* p, char villeA[], char villeB[], int etape, int* h){
    /*
    Cette fonction permet d'insérer dans l'AVL alphabétique:
        -> villeA et villeB si etape==1
        -> villeB sinon
    Cela permet d'insérer toutes les villes rencontrées dans un trajet.

    La fonction insertAVL_alphabetique s'occupe d'ajouter :
        -> +1 à nb_apparition_ville_depart_trajet si : on veut insérer villeA et etape==1
        -> +1 à nb_apparition_ville_arrivee_etape si : on veut insérer villeB et etape>=1
    */
    if (h == NULL){
        printf("Erreur 1200 : pointeur NULL.\n");
        exit(1200);
    }
    if (etape <= 0){
        printf("Erreur 1201 : numéro d'étape de trajet <= 0.\n");
        exit(1201);
    }
    /*  ville_Arrivee == 1 signifie : on insère la ville de départ du trajet (ville départ de l'étape 1)
        ville_Arrivee == 2 signifie : on insère la ville d'arrivée de toute étape >= 1
    */
    int ville_Arrivee = 0;
    if (etape == 1){
        ville_Arrivee = 1;
        p = insertAVL_alphabetique(p, villeA, ville_Arrivee, h);
    }
    ville_Arrivee = 2;
    p = insertAVL_alphabetique(p, villeB, ville_Arrivee, h);

    return p;
}


//---------------------------------- Destruction AVL ----------------------------------
void destroyAVL(Tree* p) {
    if (p != NULL) {
        destroyAVL(p->pL);
        destroyAVL(p->pR);
        free(p);
    }
}


//----------------- AFFICHAGE : préfixe, infixe -----------------
void displayPrefixe(Tree* p){
    if(p !=NULL){
        printf("[%s],   equilibre = %d,\n            nb_apparition_ville_depart_trajet = %d\n            nb_apparition_ville_arrivee_etape = %d\n            nb_apparition_par_trajet = %d\n",p->ville, p->equilibre, p->nb_apparition_ville_depart_trajet, p->nb_apparition_ville_arrivee_etape, p->nb_apparition_par_trajet);
        displayPrefixe(p->pL);
        displayPrefixe(p->pR);
    }
}

void displayInfixe(Tree* p){
    if(p !=NULL){
        displayInfixe(p->pL);
        printf("[%s],   equilibre = %d,\n            nb_apparition_ville_depart_trajet = %d\n            nb_apparition_ville_arrivee_etape = %d\n            nb_apparition_par_trajet = %d\n",p->ville, p->equilibre, p->nb_apparition_ville_depart_trajet, p->nb_apparition_ville_arrivee_etape, p->nb_apparition_par_trajet);
        displayInfixe(p->pR);
    }
}


//---------------------------------- COPIE de AVL ----------------------------------
//copie l'AVL en entier et retourne l'adresse de la racine

Tree * copie_AVL (Tree* p_origine){
    if(p_origine == NULL){
        //printf("fils NULL.\n");
        //sleep(2);
        return p_origine;
    }
    else{
        Tree * pNew = createTree("");
        if(pNew == NULL){
            printf("Erreur 1000 : pointeur NULL.\n");
            exit(1000);
        }
        strcpy(pNew->ville , p_origine->ville);    //copie du nom de ville : strcpy(destination de la copie, source);
        //printf("%s ----------> copie : %s\n", p_origine->ville, pNew->ville);
        pNew->nb_apparition_ville_depart_trajet = p_origine->nb_apparition_ville_depart_trajet;
        pNew->nb_apparition_ville_arrivee_etape = p_origine->nb_apparition_ville_arrivee_etape;
        pNew->nb_apparition_par_trajet = p_origine->nb_apparition_par_trajet;
        pNew->equilibre = 0;
        pNew->pL = copie_AVL(p_origine->pL);
        pNew->pR = copie_AVL(p_origine->pR);
        return pNew;
    }
}


//----------------- 10 VILLES LES + TRAVERSEES -----------------

/*
EXPLICATIONS et FONCTIONNEMENT:
    Le nouvel AVL va parcourir l'entièreté de l'AVL trié par ordre alphabétique de nom de ville,
    mais il sera trié par ordre numérique d'apparition de ville.
    Dans cet AVL, les doublons sont autorisés et sont mis en FILS DROIT.
    Si cet emplacement est déjà pris, il deviendra le fils gauche du fils droit ...

    EXEMPLE : Ici nous ajoutons dans l'AVL déjà formé : ville H (nb_apparition : 120)
              ville H devient fils gauche de ville G.

                                                                        ville A
                                                                    (nb_apparition : 112)

                                       ville B                                                             ville C
                                (nb_apparition : 60)                                                (nb_apparition : 120)

                       ville D                       ville E                           ville F                                ville G
                (nb_apparition : 50)            (nb_apparition : 60)            (nb_apparition : 110)                  (nb_apparition : 122)

                                                                                                                        ville H      <-------- VILLE AJOUTEE
                                                                                                                (nb_apparition : 120)



    Si on ajoute ville I (nb_apparition : 120), ce sera le fils droit de ville H.
    Pour avoir les 10 villes les plus parcourues, on fait un parcours INFIXE INVERSE (Fils Droit, puis Racine, puis Fils Gauche).
*/

Tree * transforme_AVLalphab_en_AVLnum(Tree* p_alphabetique, Tree* p_numerique, int* h){
    /* int* h est utile pour la fonction insertAVL_numerique, il permet de rééquilibrer l'AVL numérique à chaque nouvelle insertion de noeud

    ATTENTION : en appelant transforme_AVLalphab_en_AVLnum, l'adresse de h (mis en paramètre) ne doit pas être nul.
    Avant l'appel de la fonction, écrire : int* h = malloc(sizeof(int));
    Après l'appel de la fonction, écrire : free(h);
    */
    if (h == NULL){
        printf("Erreur 1000 : pointeur NULL.\n");
        exit(1000);
    }
    if(p_alphabetique == NULL){
        //do nothing
    }
    else{
        //copie du noeud p_alphabetique sans ses enfants
        Tree * pNew = createTree("");
        if(pNew == NULL){
            printf("Erreur 1010 : pointeur NULL.\n");
            exit(1010);
        }
        strcpy(pNew->ville , p_alphabetique->ville);    //copie du nom de ville : strcpy(destination de la copie, source);

        pNew->nb_apparition_ville_depart_trajet = p_alphabetique->nb_apparition_ville_depart_trajet;
        pNew->nb_apparition_ville_arrivee_etape = p_alphabetique->nb_apparition_ville_arrivee_etape;
        pNew->nb_apparition_par_trajet = p_alphabetique->nb_apparition_par_trajet;
        pNew->equilibre = 0;
        pNew->pL = NULL;
        pNew->pR = NULL;

        //insertion du noeud copié dans p_numerique (l'AVL trié par nb d'apparition de ville)
        p_numerique = insertAVL_numerique(p_numerique, pNew, h);

        //Récursivité fonction
        p_numerique = transforme_AVLalphab_en_AVLnum(p_alphabetique->pL, p_numerique, h);
        p_numerique = transforme_AVLalphab_en_AVLnum(p_alphabetique->pR, p_numerique, h);
    }
    //Cas où p_numerique est NULL au départ
    return p_numerique;
}

Tree * DIX_Villes_plus_traversees(Tree* AVL_alphabetique){
    if(AVL_alphabetique == NULL){
        printf("Erreur 1050 : pointeur NULL.\n");
        exit(1050);
    }

    //copie de AVL_alphabetique
    printf("\n             Avant copie AVL alphabetique\n");
    sleep(2);
    Tree* copie_AVL_alphabetique = NULL;
    copie_AVL_alphabetique = copie_AVL(AVL_alphabetique);
    printf("\n             Après copie AVL alphabetique\n");
    sleep(2);
    if(copie_AVL_alphabetique == NULL){
        printf("Erreur 1051 : pointeur NULL.\n");
        exit(1051);
    }

    printf("\n\n             AFFICHAGE copie_AVL_alphabetique\n\n");
    sleep(2);
    displayInfixe(copie_AVL_alphabetique);
    printf("\n\n             FIN AFFICHAGE copie_AVL_alphabetique\n\n");
    sleep(2);

    //Création de AVL_numerique (à partir de copie_AVL_alphabetique)
    Tree* AVL_numerique = NULL;
    int* h = malloc(sizeof(int));
    AVL_numerique = transforme_AVLalphab_en_AVLnum(copie_AVL_alphabetique, AVL_numerique , h);
    free(h);
    if(AVL_numerique == NULL){
        printf("Erreur 1060 : pointeur NULL.\n");
        exit(1060);
    }
        /*
    Tree * DIX_Villes_plus_traversees(AVL_alphabétique){
        Tree* copy = copie de (AVL_alphabétique);
        Tree* AVLnum = NULL;
        AVLnum = transforme_AVLalphab_en_AVLnum(copy);
        free(copy);
        return AVLnum;
    }
    */
    destroyAVL(copie_AVL_alphabetique);
    printf("\n\n             Mémoire de copie_AVL_alphabetique LIBERE\n\n");
    sleep(4);
    return AVL_numerique;
}



//----------------- Main -----------------
int main() {
    /*Tree* pTree = NULL;
    char ville1[SIZE] = "Paris";
    char ville2[SIZE] = "Limoges";
    char ville3[SIZE] = "Strasbourg";
    char ville4[SIZE] = "Orléans";
    char ville5[SIZE] = "Paris"; //2
    char ville6[SIZE] = "Toulouse";
    char ville7[SIZE] = "Strasbourg";//2
    char ville8[SIZE] = "Orléans";//2
    char ville9[SIZE] = "Strasbourg";//3

    printf("ENONCE des villes à placer dans l'AVL :\n");
    char* tab[9] = {ville1, ville2, ville3, ville4, ville5, ville6, ville7, ville8, ville9};
    for (int i=0 ; i<9 ; i++){
        printf("    %s\n", tab[i]);
    }
    printf("\nA NOTER : %s x2, %s x2, %s x3.\n\n", ville1, ville8, ville9);

    int* h = malloc(sizeof(int));

    for (int i=0; i<9 ; i++){
        printf("Insertion n°%d : %s.\n", i+1, tab[i]);
        pTree = insertAVL_alphabetique(pTree, tab[i], h);
        printf("    Préfixe:\n");
        displayPrefixe(pTree);
        printf("    Infixe:\n");
        displayInfixe(pTree);
        printf("\n\n");
    }

    free (h);
*/

    /*
    Tree* p = NULL;
    int RouteID;
    char reste_ligne[100];
    while ( scanf("%d;%99[^\n]\n", &RouteID, reste_ligne) == 2 ){
        printf("RouteID = %d      reste = %s.\n", RouteID, reste_ligne);
    }
    */

    printf("PROCESS.\n\n");

    // AVL trié par ordre alphabétique de nom de ville
    Tree* AVL_alphabetique = NULL;
    int* h = malloc(sizeof(int));
    char villeA[SIZE];
    char villeB[SIZE];
    int Route_ID;
    int etape;
    //récupération des données de data.csv
    printf("\n             CREATION AVL_alphabetique\n");
    while (scanf("%d;%d;%99[^;];%99[^\n]\n", &Route_ID, &etape, villeA, villeB) == 4){
        //printf("ville_depart = %s        ville_arrivee = %s\n        Route_ID = %d        etape = %d\n", villeA, villeB, Route_ID, etape);
        AVL_alphabetique = selection_insertAVL_alphabetique(AVL_alphabetique, villeA, villeB, etape, h);
    }
    printf("\n             FIN CREATION AVL_alphabetique\n");
    sleep(2);
    /*
    printf("\n\n    Préfixe:\n");
    displayPrefixe(p);
    */



    printf("\n\n             AFFICHAGE Infixe AVL_alphabetique\n");
    sleep(2);
    displayInfixe(AVL_alphabetique);
    printf("\n\n             FIN AFFICHAGE Infixe AVL_alphabetique\n");
    sleep(2);

    free(h);

    Tree* AVL_numerique = DIX_Villes_plus_traversees(AVL_alphabetique);

    printf("\n\n             AFFICHAGE Infixe AVL_numerique\n");
    sleep(2);
    displayInfixe(AVL_numerique);
    printf("\n\n             FIN AFFICHAGE Infixe AVL_numerique\n");
    sleep(2);
    printf("FIN.\n");

    destroyAVL(AVL_alphabetique);
    printf("\n\n             Mémoire de AVL_alphabetique LIBERE\n");
    sleep(2);
    destroyAVL(AVL_numerique);
    printf("\n\n             Mémoire de AVL_numerique LIBERE\n");
    sleep(2);

    return 0;
}