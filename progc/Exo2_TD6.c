//PréIng2 MI2 Semestre 1 TD6 Arbres AVL Exo2
#include <stdio.h>
#include <stdlib.h>

//Q1
typedef struct _tree {
    int value;
    int equilibre;      //equilibre = hauteur sous-arbre droit - hauteur sous-arbre gauche
    struct _tree* pL;
    struct _tree* pR;
} Tree;

//Q2
Tree* createTree(int v){
    Tree* pNew = malloc(sizeof(Tree));
    if(pNew == NULL){
        exit(1);
    }
    pNew->value = v;
    pNew->equilibre = 0;
    pNew->pL = NULL;
    pNew->pR = NULL;
    return pNew;
}

//Q3
Tree* RotationGauche(Tree* p){
    if (p==NULL || p->pR == NULL){
        exit(10);
    }
    Tree* pivot = p->pR;

    //Equilibrage
    if(p->equilibre == 2){
        if(pivot->equilibre == 0){
            pivot->equilibre = -1;
            p->equilibre = 1;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = 0;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 2){
            pivot->equilibre = 0;
            p->equilibre = -1;
        }
        else{
            printf("Cas non pris en charge 11. \n");
            exit(11);
        }
    }

    else if(p->equilibre == 1){
        if(pivot->equilibre == -1){
            pivot->equilibre = -2;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 0){
            pivot->equilibre = -1;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = -1;
            p->equilibre = -1;
        }
        else{
            printf("Cas non pris en charge 12. \n");
            exit(12);
        }
    }

    else{
        printf("Cas non pris en charge 13. \n");
        exit(13);
    }

    //Rotation
    p->pR = pivot->pL;
    pivot->pL = p;
    if (pivot == NULL){
        printf("Erreur 14 : pointeur NULL.\n");
        exit(14);
    }

    return pivot;
}


Tree* RotationDroite(Tree* p){
    if (p==NULL || p->pL == NULL){
        exit(15);
    }
    Tree* pivot = p->pL;

    //Equilibrage
    if(p->equilibre == -2){
        //Rotation simple 1
        if(pivot->equilibre == -1){
            pivot->equilibre = 0;
            p->equilibre = 0;
        }
        //Rotation Simple 2
        else if(pivot->equilibre == 0){
            pivot->equilibre = 1;
            p->equilibre = -1;
        }
        else if(pivot->equilibre == -2){
            pivot->equilibre = 0;
            p->equilibre = 1;
        }
        else{
            printf("Cas non pris en charge 16. \n");
            exit(16);
        }
    }

    else if(p->equilibre == -1){
        if(pivot->equilibre == -1){
            pivot->equilibre = 1;
            p->equilibre = 1;
        }
        else if(pivot->equilibre == 0){
            pivot->equilibre = 1;
            p->equilibre = 0;
        }
        else if(pivot->equilibre == 1){
            pivot->equilibre = 2;
            p->equilibre = 0;
        }
        else{
            printf("Cas non pris en charge 17. \n");
            exit(17);
        }
    }

    else{
        printf("Cas non pris en charge 18. \n");
        exit(18);
    }

    //Rotation
    p->pL = pivot->pR;
    pivot->pR = p;
    if (pivot == NULL){
        printf("Erreur 19 : pointeur NULL.\n");
        exit(19);
    }

    return pivot;
}



//Q4
Tree* DoubleRotationGauche(Tree* p){
    if (p==NULL || p->pR==NULL || p->equilibre!=2 || p->pR->equilibre!=-1){
        printf("Cas non pris en charge 20 : pointeurs NULL ou facteurs d'équilibre incorrectes.\n");
        exit(20);
    }

    p->pR = RotationDroite(p->pR);
    if (p->pR == NULL){
        printf("Erreur 21 : pointeur NULL.\n");
        exit(21);
    }
    p = RotationGauche(p);
    if (p == NULL){
        printf("Erreur 22 : pointeur NULL.\n");
        exit(22);
    }
    return p;
}



Tree* DoubleRotationDroite(Tree* p){
    if (p==NULL || p->pL==NULL || p->equilibre!=-2 || p->pL->equilibre!=1){
        printf("Cas non pris en charge 30 : pointeurs NULL ou facteurs d'équilibre incorrectes.\n");
        exit(30);
    }

    p->pL = RotationGauche(p->pL);
    if (p->pL == NULL){
        printf("Erreur 31 : pointeur NULL.\n");
        exit(31);
    }

    p = RotationDroite(p);
    if (p == NULL){
        printf("Erreur 32 : pointeur NULL.\n");
        exit(32);
    }
    return p;
}

//Q7
Tree* equilibrerAVL(Tree* p){
    if (p == NULL){
        printf("Erreur 40 : pointeur NULL.\n");
        exit(40);
    }

    if (p->equilibre == 2){
        if (p->pR == NULL){
            printf("Erreur 41 : pointeur NULL.\n");
            exit(41);
        }

        //Double rotation Gauche (RD puis RG)
        if (p->pR->equilibre == -1){
            p = DoubleRotationGauche(p);
        }

        //Rotation Simple Gauche
        else if (p->pR->equilibre >= 0){
            p = RotationGauche(p);
        }

        else {
            printf("Cas non pris en charge 42.\n");
            exit(42);
        }
    }
    else if (p->equilibre == -2) {
        if (p->pL == NULL){
            printf("Erreur 43 : pointeur NULL.\n");
            exit(43);
        }

        //Double rotation Droite (RG puis RD)
        if (p->pL->equilibre == 1){
            p = DoubleRotationDroite(p);
        }

        //Rotation Simple Droite
        else if (p->pL->equilibre <= 0){
            p = RotationDroite(p);
        }

        else {
            printf("Cas non pris en charge 44.\n");
            exit(44);
        }
    }
    else if (p->equilibre>-2 && p->equilibre<2){        //p->equilibre == -1 ou 0 ou 1. Donc équilibré.
        //do nothing
        printf("Pas besoin de rééquilibrage : equilibre = %d.\n", p->equilibre);
    }
    else{
        //do nothing
        printf("Cas non pris en charge 45 : racine de l'arbre >2 ou <-2 .\n");
        exit(45);
    }
    if (p == NULL){
        printf("Erreur 46 : pointeur NULL.\n");
        exit(46);
    }

    return p;
}

//Q8
Tree* insertAVL (Tree* p, int v, int* h){           //addABR
    //TEST
    if (h == NULL){
        printf("Erreur 50 : pointeur NULL.\n");
        exit(50);
    }

    //process
    if (p == NULL){
        p = createTree(v);
        *h = 1;
        if (p == NULL){
            printf("Erreur 51 : pointeur NULL.\n");
            exit(51);
        }
        //printf("TEST, value = %d,   eq = %d.\n", p->value, p->equilibre);
        return p;
    }
    else if (v > p->value){
        p->pR = insertAVL(p->pR, v, h);
    }
    else if (v < p->value){
        p->pL = insertAVL(p->pL, v, h);
        *h = -*h;
    }
    else {
        // We have found the same value : do nothing
    }

    if (*h != 0){
        p->equilibre = p->equilibre + *h;
        //printf("TEST, value = %d,   eq = %d.\n", p->value, p->equilibre);
        p = equilibrerAVL(p);
        if(p->equilibre == 0){
            *h = 0;
        }
        else {
            *h = 1;
        }
    }

    // return the address (may be new)
    return p;
}


// ---------- Rappel ----------------


int isEmpty(Tree* p){		//isEmpty = estVide
    return p==NULL;
}

int hasLeftChild(Tree* p){      //MAJ facteur équilibre A FAIRE
    return !isEmpty(p) && !isEmpty(p->pL);
}

int hasRightChild(Tree* p){     //MAJ facteur équilibre A FAIRE
    return !isEmpty(p) && !isEmpty(p->pR);
}

int addLeftChild(Tree* p, int v){
    if (hasLeftChild(p)){
        return 0;
    }
    p->pL = createTree(v);
    return p->pL != NULL;
}

int addRightChild(Tree* p, int v){
    if (hasRightChild(p)){
        return 0;
    }
    p->pR = createTree(v);
    return p->pR != NULL;
}

void displayPrefixe(Tree* p){
    if(p !=NULL){
        printf("[%3d], equilibre = %d\n",p->value, p->equilibre);
        displayPrefixe(p->pL);
        displayPrefixe(p->pR);
    }
}
void displayInfixe(Tree* p){
    if(p !=NULL){
        displayInfixe(p->pL);
        printf("[%3d], equilibre = %d\n",p->value, p->equilibre);
        displayInfixe(p->pR);
    }
}



// ------------------------------------


int main(){

    //Construction Arbre
    Tree* pTree = NULL;

    //Etage 0
    pTree = createTree(10);

    //Etage 1
    addLeftChild(pTree, 5);
    addRightChild(pTree, 20);

    //Etage 2
    addLeftChild(pTree->pR, 15);
    addRightChild(pTree->pR, 26);

    //Etage 3
    addLeftChild(pTree->pR->pL, 13);
    addRightChild(pTree->pR->pL, 17);

    //MAJ des équilibres des noeuds
    pTree->equilibre = 2;
    pTree->pL->equilibre = 0;
    pTree->pR->equilibre = -1;
    pTree->pR->pL->equilibre = 0;
    pTree->pR->pR->equilibre = 0;
    pTree->pR->pL->pL->equilibre = 0;
    pTree->pR->pL->pR->equilibre = 0;

    //Affichage arbre initial (non-équilibré)
    printf("Arbre initial\n");
    displayPrefixe(pTree);

    //Affichage arbre final (après rotations)
    printf("\nDouble Rotation Gauche arbre (RD puis RG)\n");
    pTree = equilibrerAVL(pTree);
    displayPrefixe(pTree);
    printf("\n");
    
    
    //Insertion 27
    printf("Insertion 27.\n");
    int* h = malloc(sizeof(int));
    pTree = insertAVL(pTree, 27, h);
    displayPrefixe(pTree);
    printf("\n");
    
    //Insertion 25
    printf("Insertion 25.\n");
    pTree = insertAVL(pTree, 25, h);
    displayPrefixe(pTree);
    printf("\n");

    //Insertion 24
    printf("Insertion 24.\n");
    pTree = insertAVL(pTree, 24, h);
    displayPrefixe(pTree);
    printf("\n");

    //Insertion 6
    printf("Insertion 6.\n");
    pTree = insertAVL(pTree, 6, h);
    displayPrefixe(pTree);
    printf("\n");
    //Insertion 1
    printf("Insertion 1.\n");
    pTree = insertAVL(pTree, 1, h);
    displayPrefixe(pTree);
    printf("\n");
    //Insertion 0
    printf("Insertion 0.\n");
    pTree = insertAVL(pTree, 0, h);
    displayPrefixe(pTree);
    printf("\n");
    //Insertion 2
    printf("Insertion 2.\n");
    pTree = insertAVL(pTree, 2, h);
    displayPrefixe(pTree);
    printf("\n");
    //Insertion 3
    printf("Insertion 3.\n");
    pTree = insertAVL(pTree, 3, h);
    displayPrefixe(pTree);
    printf("\n");

    displayInfixe(pTree);
    
    /*
    //Insertion 28
    printf("Insertion 28.\n");
    pTree = insertAVL(pTree, 28, h);
    displayPrefixe(pTree);
    printf("\n");

    //Insertion 12
    printf("Insertion 12.\n");
    pTree = insertAVL(pTree, 12, h);
    displayPrefixe(pTree);
    printf("\n");

    //Insertion 11
    printf("Insertion 11.\n");
    pTree = insertAVL(pTree, 11, h);
    displayPrefixe(pTree);
    printf("\n");
    
    //Insertion 6
    printf("Insertion 6.\n");
    pTree = insertAVL(pTree, 6, h);
    displayPrefixe(pTree);
    printf("\n");
    //Insertion 2
    printf("Insertion 2.\n");
    pTree = insertAVL(pTree, 2, h);
    displayPrefixe(pTree);
    printf("\n");
    //Insertion 3
    printf("Insertion 3.\n");
    pTree = insertAVL(pTree, 3, h);
    displayPrefixe(pTree);
    printf("\n");
    //Insertion 1
    printf("Insertion 1.\n");
    pTree = insertAVL(pTree, 1, h);
    displayPrefixe(pTree);
    printf("\n");
    //Insertion 4
    printf("Insertion 4.\n");
    pTree = insertAVL(pTree, 4, h);
    displayPrefixe(pTree);
    printf("\n");
    */
    
    free(h);
    
    /*
    //Arbre 1
    Tree* pTree1 = NULL;
    pTree1 = createTree(1);
    addRightChild(pTree1, 2);
    addRightChild(pTree1->pR, 3);
    pTree1->equilibre = 2;
    pTree1->pR->equilibre = 1;
    pTree1->pR->pR->equilibre = 0;

    //Arbre 2
    Tree* pTree2 = NULL;
    pTree2 = createTree(3);
    addLeftChild(pTree2, 2);
    addLeftChild(pTree2->pL, 1);
    pTree2->equilibre = -2;
    pTree2->pL->equilibre = -1;
    pTree2->pL->pL->equilibre = 0;

    printf("Arbre 1\n");
    displayPrefixe(pTree1);
    printf("\nArbre 2\n");
    displayPrefixe(pTree2);

    printf("\nRotation gauche arbre 1\n");
    //pTree1 = RotationGauche(pTree1);
    pTree1 = equilibrerAVL(pTree1);
    displayPrefixe(pTree1);
    printf("\n");

    printf("\nRotation droite arbre 2\n");
    //pTree2 = RotationDroite(pTree2);

    pTree2 = equilibrerAVL(pTree2);
    displayPrefixe(pTree2);
    printf("\n");
    */

    return 0;
}