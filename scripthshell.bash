#!/bin/bash

# Fonction pour afficher l'aide
afficher_aide() {
    echo " Comment utiliser: $0 [-h] data.csv d1 [d2 etc]"
    echo "Options :"
    echo "  -h      Afficher l'aide"
    echo "Traitements disponibles :"
    echo "  d1   Conducteurs avec le plus de trajets"
    echo "  d2   Conducteurs et la plus grande distance"
    echo "  l    Les 10 trajets les plus longs"
    echo "  s    Les 10 villes les plus traversées"
    echo "  t    Statistiques sur les étapes"
    # Ajouter d'autres traitements au besoin
}

# Vérifier l'option d'aide
if [ "$1" == "-h" ]; then
    afficher_aide
    exit 0
fi

# Vérifier le nombre d'arguments
if [ "$#" -lt 2 ]; then
    echo "Erreur : Au moins deux arguments sont requis."
    afficher_aide
    exit 1
fi

# Vérifier si le fichier CSV existe
if [ ! -f "data.csv" ]; then
    echo "Erreur : Le fichier CSV 'data.csv' n'existe pas."
    exit 1
fi

# Vérifier l'existence de l'exécutable C
???
# Vérifier et créer le dossier temp
dossier_temp="temp"
if [ ! -d "$dossier_temp" ]; then
    mkdir "$dossier_temp"
else
    rm -f "$dossier_temp"/*
fi

# Vérifier et créer le dossier images
dossier_images="images"
if [ ! -d "$dossier_images" ]; then
    mkdir "$dossier_images"
fi

# Switch/case pour les traitements
for traitement in "${@:2}"; do
    case "$traitement" in
        d1)
            # Traitement d1 - Conducteurs avec le plus de trajets
            gcc -o ert optiond1.c
            time cat "data.csv" | tail +2 | cut -d';' -f1,6 | sort -t';' -k1 -u | ./ert | sort -n -r | head -10
            ;;
        d2)
            # Traitement d2 - Conducteurs et la plus grande distance
            gcc -o exouma optiond2.c
            time cat "data.csv" | tail +2 | cut -d';' -f5,6 | ./exouma | sort -n -r | head -10
            ;;
        s)
            # Traitement s - Les 10 villes les plus traversées
            gcc -o traitements optionS.c
            time cat "data.csv" | tail +2 | cut -d';' -f1,5 | ./traitements | head -50
            ;;
        l)
            # Traitement l - Les 10 trajets les plus longs
            gcc -o etu optionL.c;
            time cat "data.csv" | tail +2 | cut -d';' -f1,5 | ./etu | sort -t',' -k2 -n -r | head -10 | sort -n
            ;;
        t)
            # Traitement t - Statistiques sur les étapes
            # Ajoutez votre code ici en fonction des besoins
            ;;
        *)
            echo "Erreur : Traitement '$traitement' non reconnu."
            exit 1
            ;;
    esac
done

echo "Traitements terminés avec succès."
